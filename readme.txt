=== Sent Messages for Sensei LMS ===
Contributors: meszarosrob
Tags: sensei, sensei lms, messages
Requires at least: 4.9.8
Tested up to: 5.2.1
Stable tag: 1.0.0
Requires PHP: 5.6
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Access previously sent messages from the course, lesson or quiz.

== Changelog ==

= 1.0.0 =
* Release of the plugin.